const contentDiv = document.querySelector(".content");
const indicatorStar1 = document.querySelector(".star1");
const indicatorStar2 = document.querySelector(".star2");
const indicatorStar3 = document.querySelector(".star3");

let slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  let slidesArray = document.getElementsByClassName("mySlides");
  let stars = document.getElementsByClassName("demo");
  if (n > slidesArray.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = slidesArray.length }
  for (let i = 0; i < slidesArray.length; i++) {
    slidesArray[i].style.display = "none";
  }

  for (i = 0; i < stars.length; i++) {
    stars[i].children[0].src = "assets/img/Star.png";
  }

  slidesArray[slideIndex - 1].style.display = "block";
  stars[slideIndex - 1].children[0].src = "assets/img/Star_full.png";
}

contentDiv.addEventListener('click', (evt) => {

  console.log("Click");
  if (evt.target.className === "slide-btn-rgh") {
    console.log("Direita");
    plusDivs(1);
  }
  else if (evt.target.className === "slide-btn-lft") {
    console.log("Esquerda");
    plusDivs(-1);
  }
});

indicatorStar1.addEventListener('click', () => {
  currentDiv(1)
});
indicatorStar2.addEventListener('click', () => {
  currentDiv(2)
});
indicatorStar3.addEventListener('click', () => {
  currentDiv(3)
});
